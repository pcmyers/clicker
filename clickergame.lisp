(defvar *player* "Default")
(defvar *location* "The BEANS!")
(defvar *inventory* '("knife" "water" "rope" "bugspray"))
(defvar *xcoord* 50)
(defvar *ycoord* 50)


(defun describe-surroundings (location)
  (format t "Around you, you see:")
  (format t location))

(defun describe-inventory ()
  (format t "In your bag, you have: ")
  (format t "~a:" *inventory*))

(defun explore-north ()
  (setf *ycoord* (+ *xcoord* 1)) 
  (describeloc))

(defun explore-west ()
  (setf *xcoord* (+ *xcoord* 1)) 
  (describeloc))


(defun explore-east ()
  (setf *xcoord* (- *xcoord* 1)) 
  (describeloc))


(defun explore-south ()
  (setf *ycoord* (- *ycoord* 1)) 
  (describeloc))


(defun describeloc ()
  (format t "your X coordinate: ~d~%" *xcoord*)
  (format t "Your Y coordinate: ~d~% " *ycoord*))


(defun noexplore()
    (if(yes-or-no-p "Do you want to inspect your bag?")(checkbag) 
     (if(yes-or-no-p "Do you want to use an item?") (useitem)
	(if(yes-or-no-p "Do you want to quit?") (quitout)
	(takeinput)))))

(defun quitout()
(format t "Thanks for playing!"))

(defun checkbag()
  (describe-inventory)
  (format t "To use an item, try (use-item x)"))

(defun useitem ()
  (format t "use your crap!"))

(defun play (name)
  (setf *player* name)
  (format t "Welcome. ~%")
  (format t "You are signed in as: ")
  (format t *player*)
  (takeinput))
 

